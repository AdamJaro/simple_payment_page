<?php
// please paste here a valid API KEY, unless that the code won't work!
define("API_KEY", 'API_KEY_HERE!!');
define("FROM", "HUF");
define("TO", "EUR");

include 'validator.php';
include 'converter.php';

error_reporting(E_NOTICE);
// the two function is embedded in the same page; with these flags we can control, which one do we want
$is_form_active = true;
$is_second_page_active = false;
$errors = [];

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $ccnumber = array_key_exists('ccnumber', $_POST) ? $_POST['ccnumber'] : null;
    $ccexpyear = array_key_exists('ccexpyear', $_POST) ? $_POST['ccexpyear'] : null;
    $ccexpmonth = array_key_exists('ccexpmonth', $_POST) ? $_POST['ccexpmonth'] : null;
    $amount = array_key_exists('amount', $_POST) ? $_POST['amount'] : null;

    if(!Validators::check_credit_card_number($ccnumber)) {
        $errors['ccnumber'] = "Credit card number is invalid!";
    }

    if(!Validators::check_expiration_date($ccexpmonth, $ccexpyear)) {
        $errors['ccexp'] = "Credit card expire date is invalid!";
    }

    if(!Validators::check_amount($amount)) {
        $errors['amount'] = 'Amount is invalid!';
    }

    // if every field's value was correct, we enable the second page and convert the value
    if(sizeof($errors) == 0) {
        $is_form_active = false;
        $is_second_page_active = true;
        $converter = new CurrencyConverter(API_KEY);
        $converted = $converter->convert_currency(FROM, TO, $amount);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" value="Ádám Járó">
    <title>Simple payment form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <?php if($is_form_active && !$is_second_page_active) : ?>
    <form method="post">
        <div class="form-group">
            <label for="ccnumber">Credit card number</label>
            <input type="text" name="ccnumber" id="ccnumber" pattern="[0-9]{16}" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="ccexpire">Credit card expiration date (month/year)</label>
            <div class="date-fields">
                <input type="number" name="ccexpmonth" id="ccexpmonth" min="0" max="12" class="form-control" required>
                <span class="separation">/</span>
                <input type="number" name="ccexpyear" id="ccexpyear" min="<?=date('Y')?>" max="<?=date('Y')+115?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="amount">Amount to pay</label>
            <input type="number" name="amount" id="amount" min="1" max='1000000' class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary submit-button">Submit</button>
        <?php foreach ($errors as $value) : ?>
            <div class="alert alert-primary" role="alert">
                <?=$value?>
            </div>
        <?php endforeach?>
    </form>
    <?php elseif(!$is_form_active && $is_second_page_active) : ?>
        <?php if($converted < 0) : ?>
            <div class="alert alert-primary" role="alert">There was an error with the service, please try again later!</div>
        <?php else : ?>
            <div><?=$_POST['amount']?> HUF = <?=$converter->convert_currency(FROM, TO, $amount)?> EUR.</div>
        <?php endif; ?>
    <?php endif; ?>
</body>
</html>