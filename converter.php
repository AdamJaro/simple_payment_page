<?php 
class CurrencyConverter {
    private $api_key;

    public function __construct($api_key) {
        $this->api_key = $api_key;
    }

    public function convert_currency($from, $to, $amount) {
        $from_Currency = urlencode($from);
        $to_Currency = urlencode($to);
        $query =  "{$from_Currency}_{$to_Currency}";
        // request for the currency converter service
        $json = @file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$this->api_key}");
        
        if(!$json) {
            // if there was an error with the request, return with -1. On the second page it will show an error message
            return -1;
        } else {
            // convert JSON response to PHP object
            $obj = json_decode($json, true);
            $val = floatval($obj[$query]);
            $total = $val * $amount;
            return number_format($total, 2, '.', '');
        }
    }
}
?>