<?php 
class Validators {
    public static function check_credit_card_number($ccnumber) {
        if(!isset($ccnumber) || !preg_match('/[0-9]{16}/', $ccnumber)) {
            return false;
        }
    
        // trim last digit: Luhn's algorithm will calculates this
        $work_string = strrev(substr($ccnumber, 0, -1));
        $sum = 0;
 
        // implementation of Luhn's algorithm
        for($i = 0; $i < strlen($work_string); $i++) {
            $temp = $work_string[$i];
            
            if($i % 2 == 0) {
                $temp *= 2;
        
                if($temp >= 10) {
                    $temp -= 9;
                }
            }
           
            $sum += $temp;
        }
    
        $sum += $ccnumber[strlen($work_string)];
    
        // if the "special" sum of the digits % 10 is 0, the credit card number is valid
        return $sum % 10 === 0;
    }
    
    public static function check_expiration_date($month, $year) {
        $current_year = date('Y');
        $current_month = date('m');
        $current_day = date('d');
    
        if(!isset($month) || !is_numeric($month) || $month < 0 || $month > 12) {
            return false;
        }
    
        if(!isset($year) || !is_numeric($year) || $year < $current_year || $year > ($current_year + 115)) {
            return false;
        }
    
        return $month >= $current_month && $year >= $current_year && cal_days_in_month(CAL_GREGORIAN, $month, $year) >= $current_day;
    }
    
    public static function check_amount($amount) {
        if(isset($amount) | is_numeric($amount) && $amount > 0 && $amount <= 1000000) {
            return true;
        } else {
            return false;
        }
    }
}

?>